FROM openjdk:17

ARG CHANNELFINDER_VERSION=4.7.3-alpha

ADD http://artifactory.esss.lu.se/artifactory/certificates/esss-ca01-ca.cer /etc/pki/ca-trust/source/anchors/esss-ca01-ca.crt
RUN update-ca-trust

RUN useradd -ms /bin/bash channelfinder

ADD --chown=channelfinder:channelfinder https://github.com/ChannelFinder/ChannelFinderService/releases/download/ChannelFinder-${CHANNELFINDER_VERSION}/ChannelFinder-${CHANNELFINDER_VERSION}.jar /opt/channelfinder/channelfinder.jar

USER channelfinder

CMD ["java", "-jar", "/opt/channelfinder/channelfinder.jar"]
